# build

FROM phusion/baseimage:0.11 as builder
LABEL maintainer "marco@one.io"
LABEL description="build stage for a substrate based zero node image"

ARG PROFILE=release
ARG RUNTIME=substrate

WORKDIR /substrate
COPY . /substrate

RUN apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y cmake pkg-config libssl-dev git gcc build-essential clang libclang-dev

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y && \
	export PATH=$PATH:$HOME/.cargo/bin && \
	rustup default stable && \
	rustup update nightly && \
	rustup target add wasm32-unknown-unknown --toolchain nightly && \
	cargo build --$PROFILE

# pack image

FROM phusion/baseimage:0.11
LABEL maintainer "marco@one.io"
LABEL description="packing stage for a substrate based zero node image"

ARG PROFILE=release
ARG RUNTIME=substrate

COPY --from=builder /$RUNTIME/target/$PROFILE/$RUNTIME /usr/local/bin

RUN mv /usr/share/ca* /tmp && \
	rm -rf /usr/share/*  && \
	mv /tmp/ca-certificates /usr/share/ && \
	rm -rf /usr/lib/python* && \
	useradd -m -u 1000 -U -s /bin/sh -d /$RUNTIME $RUNTIME && \
	mkdir -p /$RUNTIME/.local/share/$RUNTIME && \
	chown -R $RUNTIME:$RUNTIME /$RUNTIME/.local && \
	ln -s /$RUNTIME/.local/share/$RUNTIME /data && \
	rm -rf /usr/bin /usr/sbin

USER $RUNTIME
EXPOSE 30333 9933 9944
VOLUME ["/data"]

CMD ["/usr/local/bin/$RUNTIME"]
