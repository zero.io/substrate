
build:
	cargo build --release

run:
	./target/release/substrate --dev

reset:
	./target/release/substrate purge-chain --dev -y

build-docker:
	mkdir -p ./tmp
	cp ./target/release/substrate ./tmp
	docker build -f Dockerfile.local -t 2075/substrate:dev .

run-docker:
	docker run \
	--restart unless-stopped \
	-d \
	-p 30333:30333 \
	-p 9933:9933 \
	-v ${PWD}/data/subzero-alpha:/data:rw \
	2075/substrate:dev \
	subzero \
	--chain local \
	--name "subzero——alpha" \
